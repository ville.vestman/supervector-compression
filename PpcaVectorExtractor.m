classdef PpcaVectorExtractor < handle
    
% I-vector extractor for probabilistic principal component analysis (PPCA) based supervector compression.
% The parameters of the constructor are provided by trainPpcaTvm.m, trainPplsTvm.m, or trainSppcaTvm.m
%
% Methods:
%
% PpcaVectorExtractor(V, sigma, bias) (constructor)
% Inputs:   - Total variability matrix (V)
%           - Variance of error term (sigma)
%           - Bias term, usually the mean of training supervectors or the UBM supervector (bias)
%
% extract(superVectors)
% Input:    - Supervectors (as column vectors) to be compressed to i-vector form.
    
    properties (Access = private)
        extractorMatrix
        bias
    end
    
    methods
        
        function obj = PpcaVectorExtractor(V, sigma, bias)                  
            obj.bias = bias;
            [~, dim] = size(V);
            B = V' * 1/sigma;          
            Sigma = inv(eye(dim) + B*V);           
            obj.extractorMatrix = Sigma*B;           
        end
                
        function iVectors = extract(obj, superVectors)          
            fprintf('Extracting %d i-vectors...\n', size(superVectors, 2));
            iVectors = obj.extractorMatrix * (bsxfun(@minus, superVectors, obj.bias));
        end
        
    end
    

end
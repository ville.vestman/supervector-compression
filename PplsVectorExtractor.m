classdef PplsVectorExtractor < handle

% I-vector extractor for probabilistic partial least squares (PPLS) based supervector compression.
% The parameters of the constructor are provided by trainPplsTvm.m or trainSppcaTvm.m
% However, it is recommended to use PpcaVectorExtractor with the above functions instead of this one.
%
% Methods:
%
% PplsVectorExtractor(V, Q, sigma, rho, bias, speakerBias) (constructor)
% Inputs:   - Total variability matrix (V)
%           - matrix for mapping speaker-dependent supervectors to i-vectors (Q)
%           - Variance of error term (sigma)
%           - Variance of error term in speaker-dependent supervector model (rho)
%           - Bias term, usually the mean of training supervectors or the UBM supervector (bias)
%           - Mean of speaker-dependent supervectors (speakerBias)
%
% extract(superVectors)
% Input:    - Supervectors (as column vectors) to be compressed to i-vector form.
    
    properties (Access = private)
        extractorMatrix;
        labelMatrix
        my
        m
        muMY
    end
    
    methods
        function obj = PplsVectorExtractor(V, Q, sigma, rho, bias, speakerBias)
                        
            obj.my = speakerBias;
            obj.m = bias;
            obj.muMY = [bias; speakerBias];
            
            
            [superDim, dim] = size(V);
     
            A = [V; Q];
            B = A';
            B = [B(:, 1:superDim) * 1/sigma, B(:, superDim+1:end) * 1/rho];          
            sigmaW = inv(eye(dim) + B*A);           
            obj.extractorMatrix = sigmaW*B;
            
            C = V'*V + sigma * eye(dim);          
            obj.labelMatrix = Q * (C\(V'));
            
        end
        
        
        function iVectors = extract(obj, superVectors)
            
            fprintf('Extracting %d i-vectors...\n', size(superVectors, 2));  
            
            centeredM = bsxfun(@minus, superVectors, obj.m);            
            labels = bsxfun(@plus, obj.labelMatrix * centeredM, obj.my);
            iVectors = obj.extractorMatrix * (bsxfun(@minus, [superVectors; labels], obj.muMY));
            
        end
        
    end
    

end
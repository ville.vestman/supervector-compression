## Supervector compression methods

This package contains MATLAB routines for compressing supervectors into smaller dimensional speaker embeddings
using probabilistic principal component analysis (PPCA), factor analysis (FA),
probabilistic partial least squares (PPLS), and supervised PPCA (SPPCA).
(See usage_example.m)

The code has been tested with MATLAB R2014b.
While you are free to use and distribute the code, the authors take no reponsibility 
for any bugs or misuses of it.

If you find this code package useful and would like to cite it in your work, 
please cite the paper describing the methods:

```bibtex
@inproceedings{vestman2018supervector,
  author={Ville Vestman and Tomi Kinnunen},
  title={Supervector Compression Strategies to Speed up I-Vector System Development},
  year=2018,
  booktitle={Proc. Odyssey 2018 The Speaker and Language Recognition Workshop},
  pages={357--364},
  doi={10.21437/Odyssey.2018-50},
  url={http://dx.doi.org/10.21437/Odyssey.2018-50}
}
```

See also:

[1] Tipping, M. E., & Bishop, C. M. (1999). Probabilistic principal component analysis. 
Journal of the Royal Statistical Society: Series B (Statistical Methodology), 
61(3), 611-622.

[2] Lei, Y., & Hansen, J. H. (2010). Speaker recognition using supervised probabilistic
 principal component analysis. Interspeech 2010, 382-385.

[3] Chen, C., Han, J., & Pan, Y. (2017). Speaker Verification via Estimating Total 
Variability Space Using Probabilistic Partial Least Squares. Interspeech 2017, 1537-1541.




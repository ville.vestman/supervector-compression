function [V, sigma] = trainFaTvm(M, bias, dim, nIterations, maxPrinciple)

% Total variability model training using factor analysis (FA).
% Outputs can be used with FaVectorExtractor.
%
% Inputs:   M - matrix containing training supervectors in columns
%           bias - usually mean of training supervectors or the UBM supervector
%           dim - i-vector dimension
%           nIterations - number of iterations in training
%           maxPrinciple - 'traditional' or 'simple'
%
% Outputs:  V - total variability matrix
%           sigma - variances of error term

rng('default');
rng(1);

fprintf('\nTVM training using FA!\n');
fprintf('Initializing...\n');

[superDim, U] = size(M);
V = rand(superDim, dim);
sigma = ones(superDim, 1);
centeredM = bsxfun(@minus, M, bias);
variancesM = sum(centeredM.^2, 2) / U;

fprintf('Iterating...\n');
totalTimer = tic;

for i = 1:nIterations
    
    iterationTimer = tic;
    
    B = bsxfun(@rdivide, V', sigma');    
    Sigma = inv(eye(dim) + B*V);   
    my = Sigma*B*centeredM;
    
    if strcmp(maxPrinciple, 'traditional')
        sumEmm = U*Sigma + my*my';
    elseif strcmp(maxPrinciple, 'simple')
        sumEmm = my*my';
    end
    sumEmmInv = inv(sumEmm);
    
    V = centeredM * my' * sumEmmInv;
    sigma = variancesM - sum(V .* (V * sumEmm), 2) / U;

    iterationTime = toc(iterationTimer);
    fprintf('Iteration %2d (elapsed time = %0.2f s)\n', i, iterationTime);
    
end

totalElapsed = toc(totalTimer);
fprintf('Training completed in %0.2f seconds!\n', totalElapsed);

end


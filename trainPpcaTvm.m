function [V, sigma] = trainPpcaTvm(M, bias, dim, nIterations, maxPrinciple)

% Total variability model training using PPCA.
% Outputs can be used with PpcaVectorExtractor.
%
% Inputs:   M - matrix containing training supervectors in columns
%           bias - usually mean of training supervectors or the UBM supervector
%           dim - i-vector dimension
%           nIterations - number of iterations in training
%           maxPrinciple - 'traditional' or 'simple'
%
% Outputs:  V - total variability matrix
%           sigma - variance of error term

rng('default');
rng(1);

fprintf('\nTVM training using PPCA!\n');
fprintf('Initializing...\n');

[superDim, U] = size(M);
V = rand(superDim, dim);
sigma = 1;
centeredM = bsxfun(@minus, M, bias);
varianceM = sum(sum(centeredM.^2)) / (U * superDim);

fprintf('Iterating...\n');
totalTimer = tic;

for i = 1:nIterations
    
    iterationTimer = tic;
    
    B = V' * 1/sigma;    
    Sigma = inv(eye(dim) + B*V);    
    my = Sigma*B*centeredM;
    
    if strcmp(maxPrinciple, 'traditional')
        sumEmm = U*Sigma + my*my';
    elseif strcmp(maxPrinciple, 'simple')
        sumEmm = my*my';
    end
    sumEmmInv = inv(sumEmm);
    
    V = centeredM * my' * sumEmmInv;    
    sigma = varianceM - sum(sum(sumEmm .* (V'*V))) / (superDim * U);
   
    iterationTime = toc(iterationTimer);
    fprintf('Iteration %2d, sigma = %6.5f (elapsed time = %0.2f s)\n', i, sigma, iterationTime);
    
end

totalElapsed = toc(totalTimer);
fprintf('Training completed in %0.2f seconds!\n', totalElapsed);

end


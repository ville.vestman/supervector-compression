function [V, Q, sigma, rho, speakerBias] = trainPplsTvm(M, bias, labels, dim, nIterations, maxPrinciple, beta)

% Total variability model training using PPLS.
% Outputs can be used with PpcaVectorExtractor (recommended) or PplsVectorExtractor.
%
% Inputs:   M - (h x U) matrix containing training supervectors in columns
%           bias - usually mean of training supervectors or the UBM supervector
%           labels - U-dimensional vector containing speaker labels for each supervector given in M. Labels are assumed to run from 1 to s (s = number of speakers).
%           dim - i-vector dimension
%           nIterations - number of iterations in training
%           maxPrinciple - 'traditional' or 'simple'
%           beta - a weight parameter (use beta = 1 as a default)
%
% Outputs:  V - total variability matrix
%           Q - matrix for mapping speaker label vectors to i-vectors
%           sigma - variance of error term
%           rho - variance of error term in speaker label model
%           speakerBias - mean of speaker label vectors (or a prior distribuition of speakers)

rng('default');
rng(1);

fprintf('\nTVM training using PPLS!\n');
fprintf('Initializing...\n');

Y = full(ind2vec(labels));
speakerBias = mean(Y, 2);
[superDim, U] = size(M);
nSpeakers = size(Y, 1);
V = rand(superDim, dim);
Q = rand(nSpeakers, dim);
sigma = 1;
rho = 1;
centeredM = bsxfun(@minus, M, bias);
centeredY = bsxfun(@minus, Y, speakerBias);
varianceM = sum(sum(centeredM.^2)) / (U * superDim);
varianceY = sum(sum(centeredY.^2)) / (U * nSpeakers);

fprintf('Iterating...\n');
totalTimer = tic;

for i = 1:nIterations
    
    iterationTimer = tic;

    B = V' * 1/sigma;
    C = Q' * beta * 1/rho;
    
    Sigma = inv(eye(dim) + B*V + C*Q);  
    my = Sigma * (B*centeredM + C*centeredY);
    
    if strcmp(maxPrinciple, 'traditional')
        sumEmm = U*Sigma + my*my';
    elseif strcmp(maxPrinciple, 'simple')
        sumEmm = my*my';
    end
    sumEmmInv = inv(sumEmm);
   
    V = centeredM * my' * sumEmmInv;
    Q = centeredY * my' * sumEmmInv;
    
    sigma = varianceM - sum(sum(my .* (V' * centeredM))) / (superDim*U);
    rho = varianceY - sum(sum(my .* (Q' * centeredY))) / (nSpeakers*U);
        
    iterationTime = toc(iterationTimer);
    fprintf('Iteration %2d, sigma = %6.5f, (elapsed time = %0.2f s)\n', i, sigma, iterationTime);
    
end

totalElapsed = toc(totalTimer);
fprintf('Training completed in %0.2f seconds!\n', totalElapsed);

end


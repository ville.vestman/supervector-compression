function [V, Q, sigma, rho, speakerBias] = trainSppcaTvm(M, bias, speakerVectors, labels, dim, nIterations, maxPrinciple, beta)

% Total variability model training using SPPCA.
% Outputs can be used with PpcaVectorExtractor (recommended) or PplsVectorExtractor.
%
% Inputs:   M - (h x U) matrix containing training supervectors in columns
%           bias - usually mean of training supervectors or the UBM supervector
%           speakerVectors - (h x s) matrix containing speaker dependent supervectors for speakers from 1 to s (in this order).
%           labels - U-dimensional vector containing speaker labels for each supervector given in M. Labels are assumed to run from 1 to s.
%           dim - i-vector dimension
%           nIterations - number of iterations in training
%           maxPrinciple - 'traditional' or 'simple'
%           beta - a weight parameter (use beta = 1 as default)
%
% Outputs:  V - total variability matrix
%           Q - matrix for mapping speaker-dependent supervectors to i-vectors
%           sigma - variance of error term
%           rho - variance of error term in speaker-dependent supervector model
%           speakerBias - mean of speaker-dependent supervectors

rng('default');
rng(1);

fprintf('\nTVM training using SPPCA!\n');
fprintf('Initializing...\n');

[superDim, U] = size(M);
V = rand(superDim, dim);
Q = rand(superDim, dim);
sigma = 1;
rho = 1;
centeredM = bsxfun(@minus, M, bias);
centeredY = speakerVectors(:, labels);
speakerBias = mean(centeredY, 2);
centeredY = bsxfun(@minus, centeredY, speakerBias);
varianceM = sum(sum(centeredM.^2)) / (U * superDim);
varianceY = sum(sum(centeredY.^2)) / (U * superDim);

fprintf('Iterating...\n');
totalTimer = tic;

for i = 1:nIterations
    
    iterationTimer = tic;

    B = V' * 1/sigma;
    C = Q' * beta * 1/rho;
    
    Sigma = inv(eye(dim) + B*V + C*Q);  
    my = Sigma * (B*centeredM + C*centeredY);
    
    if strcmp(maxPrinciple, 'traditional')
        sumEmm = U*Sigma + my*my';
    elseif strcmp(maxPrinciple, 'simple')
        sumEmm = my*my';
    end
    sumEmmInv = inv(sumEmm);
   
    V = centeredM * my' * sumEmmInv;
    Q = centeredY * my' * sumEmmInv;
    
    sigma = varianceM - sum(sum(my .* (V' * centeredM))) / (superDim*U);
    rho = varianceY - sum(sum(my .* (Q' * centeredY))) / (superDim*U);
        
    iterationTime = toc(iterationTimer);
    fprintf('Iteration %2d, sigma = %6.5f, (elapsed time = %0.2f s)\n', i, sigma, iterationTime);
    
end

totalElapsed = toc(totalTimer);
fprintf('Training completed in %0.2f seconds!\n', totalElapsed);

end
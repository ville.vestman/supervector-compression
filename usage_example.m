% If you find this code package useful and would like to cite it in your work, 
% please cite the paper describing the methods:
% 
% [1] Ville Vestman and Tomi Kinnunen, "Supervector Compression Strategies to Speed up
% i-vector System Development", submitted to Speaker Odyssey 2018.
% 
% See also:
% 
% [2] Tipping, M. E., & Bishop, C. M. (1999). Probabilistic principal component analysis. 
% Journal of the Royal Statistical Society: Series B (Statistical Methodology), 
% 61(3), 611-622.
% 
% [3] Lei, Y., & Hansen, J. H. (2010). Speaker recognition using supervised probabilistic
%  principal component analysis. Interspeech 2010, 382-385.
% 
% [4] Chen, C., Han, J., & Pan, Y. (2017). Speaker Verification via Estimating Total 
% Variability Space Using Probabilistic Partial Least Squares. Interspeech 2017, 1537-1541.


clear;

model = 'ppca'; % ppca, fa, ppls, or sppca
supervisedExtractor = 'ppca'; % ppca or ppls. ppca recommended. High memory consuption with ppls! Both methods lead to same results.
maxPrinciple = 'traditional'; % traditional or simple
beta = 1; % weight parameter in ppls and sppca training
iterations = 5;
ivecDim = 10; % i-vector dimensionality

superDim = 10000; % supervector dimensionality
nDevelopmentVectors = 1000;
nDevelopmentSpeakers = 30;
nEvaluationVectors = 200;

rng(1); % fixing random seed
developmentSupervectors = rand(superDim, nDevelopmentVectors); % random supervectors for development (replace with real data)
speakerVectors = rand(superDim, nDevelopmentSpeakers); % random speaker-dependent supervectors (replace with real data)
evaluationSupervectors = rand(superDim, nEvaluationVectors); % random supervectors for evaluation (replace with real data)
labels = [1:nDevelopmentSpeakers, randi(nDevelopmentSpeakers, 1, nDevelopmentVectors - nDevelopmentSpeakers)]; % random labels (at least 1 utterance for every speaker) (replace with real labels)


bias = mean(developmentSupervectors, 2);


%% Train TVM & initialize extractor

if strcmp(model, 'ppca')
    
    [V, sigma] = trainPpcaTvm(developmentSupervectors, bias, ivecDim, iterations, maxPrinciple);   
    vectorExtractor = PpcaVectorExtractor(V, sigma, bias);
    
elseif strcmp(model, 'fa')
    
    [V, sigma] = trainFaTvm(developmentSupervectors, bias, ivecDim, iterations, maxPrinciple);
    vectorExtractor = FaVectorExtractor(V, sigma, bias);
    
else
    
    if strcmp(model, 'ppls')
        [V, Q, sigma, rho, speakerBias] = trainPplsTvm(developmentSupervectors, bias, labels, ivecDim, iterations, maxPrinciple, beta);
    elseif strcmp(model, 'sppca')
        [V, Q, sigma, rho, speakerBias] = trainSppcaTvm(developmentSupervectors, bias, speakerVectors, labels, ivecDim, iterations, maxPrinciple, beta);
    end
    
    if strcmp(supervisedExtractor, 'ppca')
        vectorExtractor = PpcaVectorExtractor(V, sigma, bias);
    elseif strcmp(supervisedExtractor, 'ppls')
        vectorExtractor = PplsVectorExtractor(V, Q, sigma, rho, bias, speakerBias);
    else
        vectorExtractor = 0; % error!
    end
    
end

%% I-vector extraction
evaluationIVectors = vectorExtractor.extract(evaluationSupervectors);

